package kz.iitu.recipeapp.recipeapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

import kz.iitu.recipeapp.recipeapp.model.Recipe;

@CrossOrigin(origins = "http://localhost:4200")
public interface RecipeRepository extends CrudRepository<Recipe, Long> {
  List<Recipe> findByName(String name);
  List<Recipe> findByIngredientsId(long id);
  List<Recipe> findByCategoriesId(long id);
  
  @Query("select recipe.ingredients from Recipe recipe")
  Iterable<Recipe> findIngredientsById(long id);

  @Query("select recipe.categories from Recipe recipe")
  Iterable<Recipe> findCategoriesById(long id);
}