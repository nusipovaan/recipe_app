package kz.iitu.recipeapp.recipeapp.model;

import lombok.Data;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "recipe")
public class Recipe {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
	
	@Column(name = "name")
    private String name;
	
	@Column(name = "servings")
    private int servings;
	
	@Column(name = "calories")
    private int calories;
	
	@ManyToMany
    private List<Ingredients> ingredients;
	
	@Column(name = "howTo")
    private String howTo;

	@ManyToMany
	private  List<Category> categories;
    
	public Recipe() {
	}

	public Recipe(String name, int servings, int calories, List<Ingredients> ingredients, String howTo, List<Category> categories) {
		this.name = name;
		this.servings = servings;
		this.calories = calories;
		this.ingredients = ingredients;
		this.howTo = howTo;
		this.categories = categories;
	}

}