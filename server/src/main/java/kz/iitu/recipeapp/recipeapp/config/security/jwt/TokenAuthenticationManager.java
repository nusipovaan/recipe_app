package kz.iitu.recipeapp.recipeapp.config.security.jwt;

import io.jsonwebtoken.impl.DefaultClaims;
import kz.iitu.recipeapp.recipeapp.config.security.jwt.TokenAuthentication;
import kz.iitu.recipeapp.recipeapp.exceptions.CustomAuthenticationException;
import kz.iitu.recipeapp.recipeapp.service.token.TokenService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public class TokenAuthenticationManager implements AuthenticationManager {

    private final UserDetailsService userDetailsService;
    private final TokenService tokenService;


    public TokenAuthenticationManager(@Qualifier("userDetailsServiceImpl") UserDetailsService userDetailsService, TokenService tokenService) {
        this.userDetailsService = userDetailsService;
        this.tokenService = tokenService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        return processAuthentication((TokenAuthentication) authentication);
    }

    public TokenAuthentication processAuthentication(TokenAuthentication authentication) throws AuthenticationException {
        String token = authentication.getToken();
        if (!tokenService.tokenValidation(token))
            throw new AuthenticationServiceException("Bad or expired token");
        return buildFullTokenAuthentication(authentication, token);
    }

    private TokenAuthentication buildFullTokenAuthentication(TokenAuthentication authentication, String token) {
        DefaultClaims claims = tokenService.getClaimsFromToken(token);
        UserDetails userDetails = userDetailsService.loadUserByUsername(claims.get("username", String.class));
        if (!userDetails.isAccountNonLocked()) {
            throw new CustomAuthenticationException("User is blocked");
        }
        else if (!userDetails.isEnabled()){
            throw new CustomAuthenticationException("User is not activated");
        }
        else {
            return new TokenAuthentication(authentication.getToken(), true, userDetails);
        }
    }

}
