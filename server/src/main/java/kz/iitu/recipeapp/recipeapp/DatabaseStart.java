package kz.iitu.recipeapp.recipeapp;

import kz.iitu.recipeapp.recipeapp.model.Category;
import kz.iitu.recipeapp.recipeapp.model.Ingredients;
import kz.iitu.recipeapp.recipeapp.repository.CategoryRepository;
import kz.iitu.recipeapp.recipeapp.repository.IngredientsRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

@Service
public class DatabaseStart implements CommandLineRunner{

	private final IngredientsRepository ingredientsRepo;
	private final CategoryRepository categoryRepository;
	
	public DatabaseStart(IngredientsRepository ingredientsRepo, CategoryRepository categoryRepository) {
		this.ingredientsRepo = ingredientsRepo;
		this.categoryRepository = categoryRepository;
	}

	@Override
	public void run(String... args) throws Exception {
		Ingredients cebola = new Ingredients("Cebola");
		Ingredients chicoria = new Ingredients("Chicoria");
		Ingredients banana = new Ingredients("Banana");
		Ingredients uva = new Ingredients("Uva");
		Ingredients leite = new Ingredients("Leite");
		Ingredients chocolate = new Ingredients("Chocolate");
		Ingredients abobora = new Ingredients("Abóbora");
		Ingredients batata = new Ingredients("Batata");
		Ingredients couveflor = new Ingredients("Couve-Flor");
		Ingredients beterraba = new Ingredients("Beterraba");
		Ingredients pimentao = new Ingredients("Pimentão");
		Ingredients feijao = new Ingredients("Feijão");
		Ingredients quiabo = new Ingredients("Quiabo");
		Ingredients lentilha = new Ingredients("Lentilha");
		Ingredients mandioca = new Ingredients("Mandioca");
		Ingredients pepino = new Ingredients("Pepino");
		Ingredients batatadoce = new Ingredients("Batata Doce");
		Ingredients chuchu = new Ingredients("Chuchu");
		Ingredients inhame = new Ingredients("Inhame");

		ingredientsRepo.save(cebola);
		ingredientsRepo.save(chicoria);
		ingredientsRepo.save(banana);
		ingredientsRepo.save(uva);
		ingredientsRepo.save(leite);
		ingredientsRepo.save(chocolate);
		ingredientsRepo.save(abobora);
		ingredientsRepo.save(batata);
		ingredientsRepo.save(couveflor);
		ingredientsRepo.save(beterraba);
		ingredientsRepo.save(pimentao);
		ingredientsRepo.save(feijao);
		ingredientsRepo.save(quiabo);
		ingredientsRepo.save(lentilha);
		ingredientsRepo.save(mandioca);
		ingredientsRepo.save(pepino);
		ingredientsRepo.save(batatadoce);
		ingredientsRepo.save(chuchu);
		ingredientsRepo.save(inhame);
		ingredientsRepo.save(abobora);

		Category main = new Category("main");
		Category breakfast = new Category("breakfast");
		Category salad = new Category("salad");
		Category appetizer = new Category("appetizer");
		Category dessert = new Category("dessert");

		categoryRepository.save(main);
		categoryRepository.save(breakfast);
		categoryRepository.save(salad);
		categoryRepository.save(appetizer);
		categoryRepository.save(dessert);




	}
}
