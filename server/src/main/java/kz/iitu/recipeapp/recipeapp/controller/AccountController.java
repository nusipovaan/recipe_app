package kz.iitu.recipeapp.recipeapp.controller;

import kz.iitu.recipeapp.recipeapp.model.dto.request.LoginRequest;
import kz.iitu.recipeapp.recipeapp.model.dto.request.RegisterRequest;
import kz.iitu.recipeapp.recipeapp.model.dto.response.TokenResponse;
import kz.iitu.recipeapp.recipeapp.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/api/account")
public class AccountController {

    private final UserService userService;

    @Autowired
    public AccountController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(value = "/login")
    public ResponseEntity<?> login(@Valid @RequestBody LoginRequest loginRequest){
        return userService.login(loginRequest);
    }

    @PostMapping(value = "/register")
    public ResponseEntity<?> register(@Valid @RequestBody RegisterRequest registerRequest){
        return userService.register(registerRequest);
    }

    @PostMapping(value = "/refresh")
    public ResponseEntity<TokenResponse> refreshToken(@RequestBody Map<String, String> refreshToken) {
        return userService.refreshToken(refreshToken.get("refreshToken"));
    }

}
