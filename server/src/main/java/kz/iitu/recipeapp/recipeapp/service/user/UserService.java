package kz.iitu.recipeapp.recipeapp.service.user;

import kz.iitu.recipeapp.recipeapp.model.User;
import kz.iitu.recipeapp.recipeapp.model.dto.request.LoginRequest;
import kz.iitu.recipeapp.recipeapp.model.dto.request.RegisterRequest;
import kz.iitu.recipeapp.recipeapp.model.dto.response.TokenResponse;
import org.springframework.http.ResponseEntity;

public interface UserService {
    User findById(String id);

    User findByEmail(String email);

    ResponseEntity<?> login(LoginRequest loginRequest);

    ResponseEntity<?> register(RegisterRequest registerRequest);

    ResponseEntity<TokenResponse> refreshToken(String token);
}
